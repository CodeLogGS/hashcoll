#!/usr/bin/env python

import random
import hashlib
import time
from subprocess import call
import datetime
import math
import concurrent.futures
import psutil

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

length = 128

def genRandomString(length_chars):
    string = ""
    for i in range(length_chars):
        if random.randint(0, 1) == 1:
            index = random.randint(65, 90)
        else:
            index = random.randint(97, 122)
        string += chr(index)
    return string

var1 = 'HyINSEnubiIDUHUbgDTPQxiQgREbRnXAidLGBDPhBKRGbSKlgaTJHrXebkXStdcjRfhuxDSSWdPwrOAFuhRAMXFhYVihhSVzRNsngdGMMeVdsAcLtbvROFCgpcshEdkz'.encode('utf-8')

print('Reference string (length: ' + str(length) + '): ' + str(var1)[2:50] + '...')

var1sum = hashlib.sha1(var1).hexdigest()
print('Reference sha1 summary: ' + var1sum)

#prompt = input('Start processing? [y/N]: ')

def printstatus(hashrate, v2, v2s, time_elapsed, i):
    tmp = call('clear', shell=True)
    print(bcolors.OKGREEN + '\nReference string: ' + str(var1)[2:50] + '...')
    print('Reference sha1 summary: ' + var1sum + bcolors.ENDC)
    print(bcolors.OKBLUE + '\nCurrent string: ' + str(v2)[2:50] + '...')
    print('Current hash: ' + str(v2s) + bcolors.ENDC)
    print(bcolors.WARNING + '\nHashrate: ' + str(hashrate) + ' KH/s')
    print('Time elapsed: ' + str(datetime.timedelta(seconds=time_elapsed)) + bcolors.ENDC)
    print('\nCPU Usage: ' + str(round(psutil.cpu_percent(), 2)))
    print('Attempts: ' + str(i))

var2 = ''
var2sum = ''

def crack(i, timestamp):

    var2 = ''
    var2sum = ''

    while not var2sum == var1sum:
        i += 1
        if i % (10000) == 0:
            timestamp2 = int(time.time())
            seconds = timestamp2 - timestamp
            try:
                hashrate = round((i / seconds) / 1000, 2)
            except (ZeroDivisionError):
                print()
                hashrate = 0
            printstatus(hashrate, var2, var2sum, seconds, i)

        var2 = str(hex(random.randint(0, math.pow(16, length))))[2:].encode('utf-8')
        var2sum =  hashlib.sha1(var2).hexdigest()

prompt = input("Start? (Y/n): ")
if prompt == 'y' or prompt == 'Y':
    print("Starting (this might break your pc)...")
    time.sleep(2)
    tmp = call('clear', shell=True)

    print('Reference string (length: ' + str(length) + '): ' + str(var1)[2:50] + '...')
    print('Reference sha1 summary: ' + var1sum)
    i = 0

    timestamp = int(time.time())
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        crack(i, timestamp)

else:
    quit()

print('FOUND MATCH: ' + str(var2))
