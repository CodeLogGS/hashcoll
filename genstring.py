import random

def genRandomString(length_chars):
    string = ""
    for i in range(length_chars):
        if random.randint(0, 1) == 1:
            index = random.randint(65, 90)
        else:
            index = random.randint(97, 122)
        string += chr(index)
    return string

print(genRandomString(128))